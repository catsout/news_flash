pub mod config;
pub mod metadata;

use self::config::AccountConfig;
use crate::feed_api::{FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::feed_api_implementations::NewsBlurMetadata;
use crate::models::{
    self, article, ArticleID, Category, CategoryID, CategoryType, FavIcon, Feed, FeedID, Headline, LoginData, Marked, PasswordLogin,
    PluginCapabilities, SyncResult, TagID, Url, NEWSFLASH_TOPLEVEL,
};
use crate::util;
use crate::util::favicon_cache::EXPIRES_AFTER_DAYS;
use crate::FatArticle;
use crate::FeedMapping;
use async_trait::async_trait;
use chrono::{DateTime, Duration, NaiveDateTime, Utc};
use failure::ResultExt;
use newsblur_api::NewsBlurApi;
use reqwest::Client;
use serde_json::{Map, Value};

pub struct NewsBlurService {
    api: Option<NewsBlurApi>,
    portal: Box<dyn Portal>,
    logged_in: bool,
    config: AccountConfig,
}

impl NewsBlurService {
    fn convert_feed_vec(raw_feeds: &Map<String, Value>, raw_folders: &[Value]) -> FeedApiResult<(Vec<Feed>, Vec<FeedMapping>, Vec<Category>)> {
        let mut mappings: Vec<FeedMapping> = Vec::new();
        let mut feeds: Vec<Feed> = Vec::new();
        let mut folders: Vec<Category> = Vec::new();

        // Add the TopLevel
        folders.push(Category {
            category_id: CategoryID::new("TopLevel"),
            label: "TopLevel".to_string(),
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            sort_index: None,
            category_type: CategoryType::Default,
        });

        for (i, folder) in raw_folders.iter().enumerate() {
            if folder.is_object() {
                let folder_object = folder.as_object().ok_or_else(|| FeedApiErrorKind::Api)?;

                for (j, fol) in folder_object.iter().enumerate() {
                    folders.push(Category {
                        category_id: CategoryID::new(fol.0),
                        label: fol.0.to_string(),
                        parent_id: NEWSFLASH_TOPLEVEL.clone(),
                        sort_index: Some((i + j) as i32),
                        category_type: CategoryType::Default,
                    });

                    for id in fol.1.as_array().ok_or_else(|| FeedApiErrorKind::Api)? {
                        let id_string = format!("{}", id.as_u64().ok_or_else(|| FeedApiErrorKind::Api)?);
                        mappings.push(FeedMapping {
                            feed_id: FeedID::new(&id_string),
                            category_id: CategoryID::new(fol.0),
                        });
                    }
                }
            } else {
                let id_string = format!("{}", folder.as_u64().ok_or_else(|| FeedApiErrorKind::Api)?);
                mappings.push(FeedMapping {
                    feed_id: FeedID::new(&id_string),
                    category_id: CategoryID::new("TopLevel"),
                });
            }
        }

        for (i, feed) in raw_feeds.iter().enumerate() {
            let website = feed.1["feed_link"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
            let feed_address = feed.1["feed_address"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
            let _icon_url = feed.1["favicon_url"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

            feeds.push(Feed {
                feed_id: FeedID::new(&feed.0.to_string()),
                label: feed.1["feed_title"].to_string(),
                website: Url::parse(&website).ok(),
                feed_url: Url::parse(&feed_address).ok(),
                icon_url: None,
                sort_index: Some(i as i32),
            });
        }

        Ok((feeds, mappings, folders))
    }
}

#[async_trait]
impl FeedApi for NewsBlurService {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::NONE)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(self.api.is_some())
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(self.logged_in)
    }

    fn user_name(&self) -> Option<String> {
        self.config.get_user_name()
    }

    fn get_login_data(&self) -> Option<LoginData> {
        if let Ok(true) = self.has_user_configured() {
            if let Some(username) = self.config.get_user_name() {
                if let Some(password) = self.config.get_password() {
                    return Some(LoginData::Password(PasswordLogin {
                        id: NewsBlurMetadata::get_id(),
                        url: self.config.get_url(),
                        user: username,
                        password,
                        http_user: None,
                        http_password: None,
                    }));
                }
            }
        }

        None
    }

    async fn login(&mut self, data: LoginData, client: &Client) -> FeedApiResult<()> {
        if let LoginData::Password(data) = data {
            let url_string = data.url.clone().ok_or(FeedApiErrorKind::Url)?;
            let url = Url::parse(&url_string).context(FeedApiErrorKind::Url)?;
            let mut api = NewsBlurApi::new(&url, &data.user, &data.password, None);
            let cookie_string = api.login(client).await.context(FeedApiErrorKind::Api)?;
            self.api = Some(api);
            self.logged_in = true;
            self.config.set_url(&url_string);
            self.config.set_password(&data.password);
            self.config.set_user_name(&data.user);
            self.config.set_cookie_string(&cookie_string);
            self.config.write()?;
            return Ok(());
        }

        self.logged_in = false;
        self.api = None;
        Err(FeedApiErrorKind::Login.into())
    }

    async fn logout(&mut self, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.logout(client).await.context(FeedApiErrorKind::Api)?;
            self.config.delete()?;
            self.logged_in = false;
            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let response = api.get_feeds(client).await.context(FeedApiErrorKind::Api)?;

            let feeds = &response["feeds"].as_object().ok_or_else(|| FeedApiErrorKind::Api)?;
            let folders = &response["folders"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

            let (feeds, mappings, folders) = NewsBlurService::convert_feed_vec(feeds, folders)?;

            let mut articles: Vec<FatArticle> = Vec::new();

            // Get the first 2 pages (12 items) of every feed
            for feed in &feeds {
                for page in 1..2 {
                    let response = api
                        .get_stories(&client, feed.feed_id.to_str(), false, page)
                        .await
                        .context(FeedApiErrorKind::Api)?;

                    let stories_array = &response["stories"].as_array();
                    let stories;
                    match stories_array {
                        Some(stry) => {
                            stories = stry;
                        }
                        None => break,
                    }

                    for story in stories.iter() {
                        let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                        let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                        let error_msg = format!("date_string: {:?}", date_string);

                        let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                            NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                        } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                            NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                        } else {
                            panic!("Can't parse data: {:?}", date_string);
                        };

                        let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                        let unread = match article_read {
                            0 => article::Read::Unread,
                            _ => article::Read::Read,
                        };

                        let article_id = format!("{}:{}", feed.feed_id, story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?);

                        let marked = match story["starred"].as_bool() {
                            Some(starred) => {
                                if starred {
                                    Marked::Marked
                                } else {
                                    Marked::Unmarked
                                }
                            }
                            None => Marked::Unmarked,
                        };

                        articles.push(FatArticle {
                            article_id: ArticleID::new(&article_id),
                            title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            feed_id: feed.feed_id.clone(),
                            url: Url::parse(&url).ok(),
                            date,
                            synced: Utc::now().naive_utc(),
                            html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            summary: None,
                            direction: None,
                            unread,
                            marked,
                            scraped_content: None,
                            plain_text: None,
                        });
                    }
                }
            }

            // Get a list of unread stories
            let response = api.get_unread_story_hashes(&client).await.context(FeedApiErrorKind::Api)?;
            let hashes = &response["unread_feed_story_hashes"].as_object().ok_or_else(|| FeedApiErrorKind::Api)?;

            let mut unread_story_hashes = Vec::new();

            for hash in hashes.iter() {
                for story in hash.1.as_array().ok_or_else(|| FeedApiErrorKind::Api)? {
                    unread_story_hashes.push(story.as_str().ok_or_else(|| FeedApiErrorKind::Api)?);
                }
            }

            for offset in 0..(unread_story_hashes.len() / 99) {
                let start = offset * 99;
                let end = (offset + 1) * 99;
                let response = if end < unread_story_hashes.len() {
                    api.get_river_stories(&client, &unread_story_hashes[start..end])
                        .await
                        .context(FeedApiErrorKind::Api)?
                } else {
                    api.get_river_stories(&client, &unread_story_hashes[start..unread_story_hashes.len()])
                        .await
                        .context(FeedApiErrorKind::Api)?
                };
                let stories = &response["stories"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

                for story in stories.iter() {
                    let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                    let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let error_msg = format!("date_string: {:?}", date_string);

                    let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                    } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                    } else {
                        panic!("Can't parse data: {:?}", date_string);
                    };

                    let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let unread = match article_read {
                        0 => article::Read::Unread,
                        _ => article::Read::Read,
                    };

                    let article_id = format!(
                        "{}:{}",
                        story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?,
                        story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?
                    );

                    let marked = match story["starred"].as_bool() {
                        Some(starred) => {
                            if starred {
                                Marked::Marked
                            } else {
                                Marked::Unmarked
                            }
                        }
                        None => Marked::Unmarked,
                    };

                    articles.push(FatArticle {
                        article_id: ArticleID::new(&article_id),
                        title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        feed_id: FeedID::new(&format!("{}", story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?)),
                        url: Url::parse(&url).ok(),
                        date,
                        synced: Utc::now().naive_utc(),
                        html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        summary: None,
                        direction: None,
                        unread,
                        marked,
                        scraped_content: None,
                        plain_text: None,
                    });
                }
            }

            // Get a list of stared stories that we haven't already retrieved.
            // Although we have already downloaded some starred stories, it's possible we will miss
            // old unread stories. This extra step ensures that we get them all.
            let response = api.get_stared_story_hashes(&client).await.context(FeedApiErrorKind::Api)?;
            let hashes = &response["starred_story_hashes"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

            let mut stared_story_hashes = Vec::new();

            for hash in hashes.iter() {
                stared_story_hashes.push(hash.as_str().ok_or_else(|| FeedApiErrorKind::Api)?);
            }

            for offset in 0..(stared_story_hashes.len() / 99) {
                let start = offset * 99;
                let end = (offset + 1) * 99;
                let response = if end < stared_story_hashes.len() {
                    api.get_river_stories(&client, &stared_story_hashes[start..end])
                        .await
                        .context(FeedApiErrorKind::Api)?
                } else {
                    api.get_river_stories(&client, &stared_story_hashes[start..stared_story_hashes.len()])
                        .await
                        .context(FeedApiErrorKind::Api)?
                };
                let stories = &response["stories"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

                for story in stories.iter() {
                    let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                    let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let error_msg = format!("date_string: {:?}", date_string);

                    let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                    } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                    } else {
                        panic!("Can't parse data: {:?}", date_string);
                    };

                    let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let unread = match article_read {
                        0 => article::Read::Unread,
                        _ => article::Read::Read,
                    };

                    let article_id = format!(
                        "{}:{}",
                        story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?,
                        story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?
                    );

                    let mut article_offset = None;
                    for (i, article) in articles.iter().enumerate() {
                        if article.article_id == ArticleID::new(&article_id) {
                            article_offset = Some(i);
                        }
                    }

                    if article_offset == None {
                        articles.push(FatArticle {
                            article_id: ArticleID::new(&article_id),
                            title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            feed_id: FeedID::new(&format!("{}", story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?)),
                            url: Url::parse(&url).ok(),
                            date,
                            synced: Utc::now().naive_utc(),
                            html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                            summary: None,
                            direction: None,
                            unread,
                            marked: Marked::Marked,
                            scraped_content: None,
                            plain_text: None,
                        });
                    }
                }
            }

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(folders),
                mappings: util::vec_to_option(mappings),
                tags: None,
                taggings: None,
                headlines: None,
                articles: util::vec_to_option(articles),
                enclosures: None,
            });
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn sync(&self, max_count: u32, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let response = api.get_feeds(client).await.context(FeedApiErrorKind::Api)?;

            let feeds = &response["feeds"].as_object().ok_or_else(|| FeedApiErrorKind::Api)?;
            let folders = &response["folders"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

            let (feeds, mappings, folders) = NewsBlurService::convert_feed_vec(feeds, folders)?;

            let mut articles: Vec<FatArticle> = Vec::new();
            let mut headlines: Vec<Headline> = Vec::new();

            // Get a list of unread stories
            let response = api.get_unread_story_hashes(&client).await.context(FeedApiErrorKind::Api)?;
            let hashes = &response["unread_feed_story_hashes"].as_object().ok_or_else(|| FeedApiErrorKind::Api)?;
            let mut unread_story_hashes = Vec::new();

            for hash in hashes.iter() {
                for story in hash.1.as_array().ok_or_else(|| FeedApiErrorKind::Api)? {
                    unread_story_hashes.push(story.as_str().ok_or_else(|| FeedApiErrorKind::Api)?);
                }
            }

            // Get a list of stared stories
            let response = api.get_stared_story_hashes(&client).await.context(FeedApiErrorKind::Api)?;
            let hashes = &response["starred_story_hashes"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

            let mut stared_story_hashes = Vec::new();

            for hash in hashes.iter() {
                stared_story_hashes.push(hash.as_str().ok_or_else(|| FeedApiErrorKind::Api)?);
            }

            // Skip downloading stories we already have
            let local_unread_ids = self.portal.get_article_ids_unread_all().context(FeedApiErrorKind::Portal)?;
            let mut unread_story_hashes_to_fetch = Vec::new();
            for id in &unread_story_hashes {
                if local_unread_ids.contains(&ArticleID::new(id)) {
                    continue;
                }
                unread_story_hashes_to_fetch.push(id.clone());
            }

            for offset in 0..((unread_story_hashes_to_fetch.len() / 99) + 1) {
                let start = offset * 99;
                let end = (offset + 1) * 99;
                let response = if end < unread_story_hashes_to_fetch.len() {
                    api.get_river_stories(&client, &unread_story_hashes_to_fetch[start..end])
                        .await
                        .context(FeedApiErrorKind::Api)?
                } else {
                    api.get_river_stories(&client, &unread_story_hashes_to_fetch[start..unread_story_hashes_to_fetch.len()])
                        .await
                        .context(FeedApiErrorKind::Api)?
                };
                let stories = &response["stories"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

                for story in stories.iter() {
                    let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                    let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let error_msg = format!("date_string: {:?}", date_string);

                    let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                    } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                    } else {
                        panic!("Can't parse data: {:?}", date_string);
                    };

                    let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let unread = match article_read {
                        0 => article::Read::Unread,
                        _ => article::Read::Read,
                    };

                    let article_id = format!(
                        "{}:{}",
                        story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?,
                        story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?
                    );

                    let marked = match story["starred"].as_bool() {
                        Some(starred) => {
                            if starred {
                                Marked::Marked
                            } else {
                                Marked::Unmarked
                            }
                        }
                        None => Marked::Unmarked,
                    };

                    articles.push(FatArticle {
                        article_id: ArticleID::new(&article_id),
                        title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        feed_id: FeedID::new(&format!("{}", story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?)),
                        url: Url::parse(&url).ok(),
                        date,
                        synced: Utc::now().naive_utc(),
                        html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        summary: None,
                        direction: None,
                        unread,
                        marked,
                        scraped_content: None,
                        plain_text: None,
                    });
                }
            }

            // Mark all other stories as read
            let local_unread_ids = self.portal.get_article_ids_unread_all().context(FeedApiErrorKind::Portal)?;
            for local_id in local_unread_ids {
                if unread_story_hashes.contains(&&local_id.to_string().as_str()) {
                    continue;
                }
                let should_mark_read_headlines = Headline {
                    article_id: ArticleID::new(&local_id.to_string()),
                    unread: article::Read::Read,
                    marked: if stared_story_hashes.contains(&local_id.to_string().as_str()) {
                        Marked::Marked
                    } else {
                        Marked::Unmarked
                    },
                };
                headlines.push(should_mark_read_headlines);
            }

            // Calculate the number of pages we should fetch, there are 6 stories per page.
            let read_sync_num = if max_count > unread_story_hashes.len() as u32 {
                (max_count - unread_story_hashes.len() as u32) / 6
            } else {
                1
            };

            // Get a list of read articles, including content
            // For these pages we retrieve all of the article information.
            for page in 1..read_sync_num {
                let response = api.get_read_stories(&client, page).await.context(FeedApiErrorKind::Api)?;
                let stories_array = &response["stories"].as_array();
                let mut page_articles: Vec<ArticleID> = Vec::new();
                let stories;
                match stories_array {
                    Some(stry) => {
                        stories = stry;
                    }
                    None => break,
                }

                for story in stories.iter() {
                    let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                    let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let error_msg = format!("date_string: {:?}", date_string);

                    let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                    } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                        NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                    } else {
                        panic!("Can't parse data: {:?}", date_string);
                    };

                    let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                    let unread = match article_read {
                        0 => article::Read::Unread,
                        _ => article::Read::Read,
                    };

                    let article_id = format!(
                        "{}:{}",
                        story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?,
                        story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?
                    );

                    let marked = match story["starred"].as_bool() {
                        Some(starred) => {
                            if starred {
                                Marked::Marked
                            } else {
                                Marked::Unmarked
                            }
                        }
                        None => Marked::Unmarked,
                    };

                    articles.push(FatArticle {
                        article_id: ArticleID::new(&article_id),
                        title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        feed_id: FeedID::new(&format!("{}", story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?)),
                        url: Url::parse(&url).ok(),
                        date,
                        synced: Utc::now().naive_utc(),
                        html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                        summary: None,
                        direction: None,
                        unread,
                        marked,
                        scraped_content: None,
                        plain_text: None,
                    });

                    // Create a list of read articles on each page.
                    if unread == article::Read::Read {
                        page_articles.push(ArticleID::new(&article_id));
                    }
                }

                // If all the read articles on this page are already downloaded
                // Then break this loop as we should have downloaded all the read articles.
                // TODO: If NewBlur adds support for read date in sync information use
                // that instead.
                if let Ok(_) = self.portal.get_articles(&page_articles) {
                    break;
                }
            }

            // Download stared stories

            // Skip downloading stories we already have
            let local_marked_ids = self.portal.get_article_ids_marked_all().context(FeedApiErrorKind::Portal)?;
            let mut marked_story_hashes_to_fetch = Vec::new();
            for id in &stared_story_hashes {
                if local_marked_ids.contains(&ArticleID::new(id)) {
                    continue;
                }
                marked_story_hashes_to_fetch.push(id.clone());
            }

            if marked_story_hashes_to_fetch.len() > 0 {
                for offset in 0..((marked_story_hashes_to_fetch.len() / 99) + 1) {
                    let start = offset * 99;
                    let end = (offset + 1) * 99;
                    let response = if end < marked_story_hashes_to_fetch.len() {
                        api.get_river_stories(&client, &marked_story_hashes_to_fetch[start..end])
                            .await
                            .context(FeedApiErrorKind::Api)?
                    } else {
                        api.get_river_stories(&client, &marked_story_hashes_to_fetch[start..marked_story_hashes_to_fetch.len()])
                            .await
                            .context(FeedApiErrorKind::Api)?
                    };
                    let stories = &response["stories"].as_array().ok_or_else(|| FeedApiErrorKind::Api)?;

                    for story in stories.iter() {
                        let url = story["story_permalink"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;
                        let date_string = story["story_date"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?;

                        let error_msg = format!("date_string: {:?}", date_string);

                        let date = if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").is_ok() {
                            NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S.%f").expect(&error_msg)
                        } else if NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").is_ok() {
                            NaiveDateTime::parse_from_str(date_string, "%Y-%m-%d %H:%M:%S").expect(&error_msg)
                        } else {
                            panic!("Can't parse data: {:?}", date_string);
                        };

                        let article_read = story["read_status"].as_i64().ok_or_else(|| FeedApiErrorKind::Api)?;

                        let unread = match article_read {
                            0 => article::Read::Unread,
                            _ => article::Read::Read,
                        };

                        let article_id = format!(
                            "{}:{}",
                            story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?,
                            story["guid_hash"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?
                        );

                        let mut article_offset = None;
                        for (i, article) in articles.iter().enumerate() {
                            if article.article_id == ArticleID::new(&article_id) {
                                article_offset = Some(i);
                            }
                        }

                        if article_offset == None {
                            articles.push(FatArticle {
                                article_id: ArticleID::new(&article_id),
                                title: Some(story["story_title"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                                author: Some(story["story_authors"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                                feed_id: FeedID::new(&format!("{}", story["story_feed_id"].as_u64().ok_or_else(|| FeedApiErrorKind::Api)?)),
                                url: Url::parse(&url).ok(),
                                date,
                                synced: Utc::now().naive_utc(),
                                html: Some(story["story_content"].as_str().ok_or_else(|| FeedApiErrorKind::Api)?.to_string()),
                                summary: None,
                                direction: None,
                                unread,
                                marked: Marked::Marked,
                                scraped_content: None,
                                plain_text: None,
                            });
                        }
                    }
                }
            }

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(folders),
                mappings: util::vec_to_option(mappings),
                tags: None,
                taggings: None,
                headlines: util::vec_to_option(headlines),
                articles: util::vec_to_option(articles),
                enclosures: None,
            });
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_article_read(&self, articles: &[ArticleID], read: models::Read, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for article in articles {
                if read == models::Read::Read {
                    api.mark_stories_read(&client, article.to_str()).await.context(FeedApiErrorKind::Api)?;
                } else {
                    api.mark_story_unread(&client, article.to_str()).await.context(FeedApiErrorKind::Api)?;
                }
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_article_marked(&self, articles: &[ArticleID], marked: models::Marked, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for article in articles {
                if marked == models::Marked::Marked {
                    api.mark_story_hash_as_starred(&client, article.to_str())
                        .await
                        .context(FeedApiErrorKind::Api)?;
                } else {
                    api.mark_story_hash_as_unstarred(&client, article.to_str())
                        .await
                        .context(FeedApiErrorKind::Api)?;
                }
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_feed_read(&self, feeds: &[FeedID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for feed in feeds {
                api.mark_feed_read(client, feed.to_str()).await.context(FeedApiErrorKind::Api)?;
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn set_category_read(&self, categories: &[CategoryID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for category_id in categories {
                let article_ids = self
                    .portal
                    .get_article_ids_unread_category(category_id)
                    .context(FeedApiErrorKind::Portal)?;
                for article in article_ids {
                    api.mark_stories_read(client, article.to_str()).await.context(FeedApiErrorKind::Api)?;
                }
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_tag_read(&self, _tags: &[TagID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn set_all_read(&self, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.mark_all_read(client).await.context(FeedApiErrorKind::Api)?;

            return Ok(());
        }
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn add_feed(
        &self,
        _url: &Url,
        _title: Option<String>,
        _category_id: Option<CategoryID>,
        _client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn remove_feed(&self, _id: &FeedID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn move_feed(&self, _feed_id: &FeedID, _from: &CategoryID, _to: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn rename_feed(&self, _feed_id: &FeedID, _new_title: &str, _client: &Client) -> FeedApiResult<FeedID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn add_category(&self, _title: &str, _parent: Option<&CategoryID>, _client: &Client) -> FeedApiResult<CategoryID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn remove_category(&self, _id: &CategoryID, _remove_children: bool, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn rename_category(&self, _id: &CategoryID, _new_title: &str, _client: &Client) -> FeedApiResult<CategoryID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn import_opml(&self, _opml: &str, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn add_tag(&self, _title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn rename_tag(&self, _id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn get_favicon(&self, feed_id: &FeedID, client: &Client) -> FeedApiResult<FavIcon> {
        if let Some(api) = &self.api {
            let response = api.favicons(&client, feed_id.to_str()).await.context(FeedApiErrorKind::Api)?;
            let favicon_data = &response[feed_id.to_str()];

            let data = match favicon_data.as_str() {
                Some(string) => Some(base64::decode(string).context(FeedApiErrorKind::Encryption)?),
                None => None,
            };

            let favicon = FavIcon {
                feed_id: feed_id.clone(),
                expires: Utc::now().naive_utc() + Duration::days(EXPIRES_AFTER_DAYS),
                format: None,
                etag: None,
                source_url: None,
                data,
            };

            return Ok(favicon);
        }
        Err(FeedApiErrorKind::Login.into())
    }
}
